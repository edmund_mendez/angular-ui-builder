# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Proof of concept for dynamically adding components to a UI using AngularJS.
Angular doesn't natively support dynamic component(directive) creation, so additional effort is needed for directives to be able to acheive this (compile function).

Efforts needed to cleanup the bootstrap html markup.

### How do I get set up? ###

No setup necessary. Just launch index.html (can be opened locally).

### Contribution guidelines ###

None at this time.

### Who do I talk to? ###

edmundm@gmail.com